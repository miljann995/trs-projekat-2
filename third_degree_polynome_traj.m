function [q_ref, dq_ref] = third_degree_polynome_traj(delta_t, tk, qi, qf)

% qi    - pocetna referentna vrednost
% qf    - krajnja referentna vrednost
% dqi   - pocetna brzina
% dqf   - krajnja brzina
% tk    - vreme simulacije

dqi = 0;
dqf = 0;

lengthT = int8(tk/delta_t+1);

q_ref = zeros(1,lengthT);    % matrica trajektorije
dq_ref = zeros(1,lengthT);   % matrica brzine

% koeficijenti polinoma
a0 = qi;
a1 = dqi;
a2 = (-3*(qi-qf)-(2*dqi+dqf)*tk)/tk^2;
a3 = (2*(qi-qf)+(dqi+dqf)*tk)/tk^3;

% planiranje trajektorije
for i = 1 : lengthT
   deltat = double(i-1)*delta_t;
   q_ref(i) = a3*deltat^3 + a2*deltat^2 + a1*deltat + a0;
end

% planiranje brzine
for i = 1 : lengthT
   deltat = double(i-1)*delta_t;
   dq_ref(i) = 3*a3*deltat^2 + 2*a2*deltat + a1;
end
