function dydt_Q_dQ = int_4DoF(t, Q_dQ)
global robot Tau
%pomocne vrednosti
q_temp=Q_dQ(1:4);
dq_temp=Q_dQ(5:8);

%racunanje odziva sistema
C=robot.coriolis(q_temp',dq_temp');
G=robot.gravload(q_temp');
H=robot.inertia(q_temp');

dydtQ_4=H\(Tau-C*(dq_temp)-G');

dydt_Q_dQ(1:4)=Q_dQ(5:8);
dydt_Q_dQ(5:8)=dydtQ_4;
dydt_Q_dQ=dydt_Q_dQ';
