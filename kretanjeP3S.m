global robot q_ref_value dq_ref_value X_plot Tau

%% Granicni polo�aji
for i = 1:4
    if q0(i) < q1(i)
        if (q1(i) - q0(i)) > joint_limits(i,2)
            q1(i) = q0(i) + joint_limits(i,2);
        end
    end
    if q0(i) > q1(i)
        if (q1(i) - q0(i)) < joint_limits(i,1)
            q1(i) = q0(i) + joint_limits(i,1);
        end
    end
end

%% Planiranje trajektorije i brzine zlgobova po trapeznom modelu

q_init = q0;   % Inicijalna pozicija
q_stop = q1;   % Krajnja pozicija
for j = 1:4
    [q_ref(:,j), dq_ref(:,j)] = third_degree_polynome_traj(dt, TkP3S/2, q_init(j), q_stop(j));
end

%% Pocetno stanje

q = q_init;
dq = [0 0 0 0];
ddq = [0 0 0 0];

%% Glavna petlja

i = 1;
t_tmp = T0;
while (t_tmp<TkP3S)
    t_tmp = (i - 1) * dt;
    q_ref_value = q_ref(i,:);
    dq_ref_value = dq_ref(i,:);
    X_mat = robot.fkine(q);
    X_value = X_mat.t';
    X_ref_mat = robot.fkine(q_ref_value);
    X_ref_value = X_ref_mat.t';
    dX_value = robot.jacob0(dq)*dq';
    dX_value = dX_value';
    dX_ref_value = robot.jacob0(dq_ref_value)*dq_ref_value';
    dX_ref_value = dX_ref_value';
    X_plot = [X_plot, X_value];
    [Tau, Tau_FF, Tau_FB, H, C, G] = upravljanje(q,dq);
    Q_dQ0 = [q';dq'];
    options = odeset('RelTol',1e-2,'AbsTol',1e-3,'MaxOrder',3);
    [tout, Q_dQ_out] = ode45(@int_4DoF,[t_tmp t_tmp+dt],Q_dQ0,options);
    Q_dQ = Q_dQ_out(end,:)';
    q = Q_dQ(1:4)';
    dq = Q_dQ(5:8)';
    ddq = (H\(Tau-C*(dq')-G'))';
    write_in_memory;
    scatter3(X_plot(:, 1), X_plot(:, 2), X_plot(:, 3),'m.');
    robot.plot(q);
    i = i + 1;
end

% iscrtavanje;