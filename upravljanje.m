function [Tau, Tau_FF, Tau_FB,H,C,G] = upravljanje(q, dq)

global q_ref_value dq_ref_value Kp1 Kd1 robot

% Matrice H,C,G
C=robot.coriolis(q,dq);
G=robot.gravload(q);
H=robot.inertia(q);

% Racunanje momenta
ukljucen_ff = 1;    % Ukljucenje (1) ili iskljucenje (0) gravitacione kompenzacije
for j = 1:4
   % Feedforward - gravitaciona kompenzacija
   Tau_FF(j,1) = G(1,j)*ukljucen_ff;
    
   % Feedbackward - PD kontroler
   Tau_FB(j,1) = Kp1(j)*(q_ref_value(j)-q(j))+Kd1(j)*(dq_ref_value(j)-dq(j));
   
   % Moment
   Tau = Tau_FF + Tau_FB;
end
