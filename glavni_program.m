clear all
close all
clc
 
global a1 a2 a3 a4 a5 robot Kp1 Kd1 X_plot Tau

%% Parametri

% Simulacioni parametri

T0 = 0;           % pocetni trenutak izrazeno u [s]
dt = 0.05;        % simulacioni korak
T = 5;            % ukupno vreme simulacije kretanja izrazeno u [s]
TkP3S = 0.3*T;    % vreme kretanja B-A-B i C-D-C
TkPTP = 0.4*T;    % vreme kretanja B-C
T_acc = 0.2*T;    % ubrzanje/usporenje je 20% ukupnog vremena
lengthT = T/dt;   % broj koraka simulacije
g = 9.81;         % gravitaciono ubrzanje, izrazeno u [m/s^2]
Fint = [0; 0; 0]; % sila

% Konstantni parametri

a1 = 0.225;    % izrazeno u [m]
% d1 = 0.301;   % izrazeno u [m]
a2 = 0.225;    % izrazeno u [m]
d4 = 0.036;    % izrazeno u [m]

% Promenljivi parametri

theta1 = 0;
theta2 = 0;
d3 = -0.1;
theta4 = 0;

% Granicne vrednosti parametara

joint_limits = [-pi/180*165 pi/180*165; 
                -pi/180*110 pi/180*110; 
                -1 0;
                -pi/180*160 pi/180*160]; 
     
%% DH parametri

% [theta, d, a, alpha, sigma]
% L1=Link([theta1, a1, 0, pi/2, 0],'standard');
% L2=Link([theta2, 0, a2, 0, 0],'standard');
% L3=Link([theta3, 0, a3, pi/2, 1],'standard');
% L4=Link([theta4, a4, 0, -pi/2, 0],'standard');

% [theta, d, a, alpha, sigma]
L1 = Link([theta1, 0, a1, 0, 0], 'standard');
L2 = Link([theta2, 0, a2, pi, 0], 'standard');
L3 = Link([0, d3, 0, 0,1], 'standard');
L4 = Link([0, d4, 0, 0,0], 'standard');


%% Dinamicki parametri

% Link 1 - Link 6

% Duzine segmenata [m]:
% L1.l = 0.225;
% L2.l = 0.225;
% L3.l = d3;
% L4.l = 0.1;

L1.m = 7.5;
L2.m = 5;
L3.m = 2.5;
L4.m = 0.5;

L1.r = [9.77E-05 -0.00012 0.23841];
L2.r = [0.00078 -0.00212 0.10124];
L3.r = [0.02281 0.00106 0.05791];
L4.r = [0.2247 0.00015 0.00041];

L1.I = [0.0142175 -1.28579E-05 -2.31364E-05; -1.28579E-05 0.0144041 1.93404E-05; -2.31364E-05 1.93404E-05 0.0104533];
L2.I = [0.0603111 9.83431E-06 5.72407E-05; 9.83431E-06 0.041569 -0.00050497; 5.72407E-05 -0.00050497 0.0259548];
L3.I = [0.00835606 -8.01545E-05 0.00142884; -8.01545E-05 0.016713 -0.000182227; 0.00142884 -0.000182227 0.0126984];
L4.I = [0.00284661 -2.12765E-05 -1.6435E-05; -2.12765E-05 0.00401346 1.31336E-05; -1.6435E-05 1.31336E-05 0.0052535];

L1.Jm = 0;
L2.Jm = 0;
L3.Jm = 0;
L4.Jm = 0;

%% Parametri PD kontrolera 

Kp1 = [3;3;3;3]';        
Kd1 = [5;5;5;5]';

%% Generisanje robota

robot=SerialLink([L1 L2 L3 L4]);
robot.name='Denso HS-4545';
X_plot=[];

%% Demo

% Tacke kretanja
% A(0.101, -0.298, -0.065)
% B(0.101, -0.298, -0.03)
% C(-0.112, 0.320, -0.03)
% D(-0.112, 0.320, -0.114)

A = SE3(0.101, -0.298, -0.4);
B = SE3(0.101, -0.298, -0.1);
C = SE3(-0.112, 0.320, -0.1);
D = SE3(-0.112, 0.320, -0.4);

% Vrednosoti unutrasnjih koordinata 
qA = robot.ikine(A, 'mask', [1, 1, 1, 0, 0, 0]);
qB = robot.ikine(B, 'mask', [1, 1, 1, 0, 0, 0]);
qC = robot.ikine(C, 'mask', [1, 1, 1, 0, 0, 0]);
qD = robot.ikine(D, 'mask', [1, 1, 1, 0, 0, 0]);

% Trajektorija od B do A
q0=qB;
q1=qA;
robot.plot(q0, 'workspace', [-0.5 0.5 -0.5 0.5 -0.5 0.5]);
kretanjeP3S;
pause(5)

% Trajektorija od A do B
q0=qA;
q1=qB;
robot.plot(q0, 'mask', [1, 1, 1, 0, 0, 0]);
kretanjeP3S;
pause(5)

% Trajektorija od B do C (point-to-point)
q0=qB;
q1=qC;
robot.plot(q0, 'mask', [1, 1, 1, 0, 0, 0]);
kretanjePTP;
pause(5)

% Trajektorija od C do D
q0=qC;
q1=qD;
robot.plot(q0, 'mask', [1, 1, 1, 0, 0, 0]);
kretanjeP3S;
pause(5)

% Trajektorija od D do C
q0=qD;
q1=qC;
robot.plot(q0, 'mask', [1, 1, 1, 0, 0, 0]);
kretanjeP3S;
pause(5)
