function [q_ref, dq_ref] = PTP_traj(delta_t, tk, qi, qf)

% qi    - pocetna referentna vrednost
% qf    - krajnja referentna vrednost
% tk    - vreme simulacije

lengthT = tk/delta_t+1;

% planiranje trajektorije
q_ref = ones(1,lengthT)*qf;

% planiranje brzine
dq_ref = zeros(1,lengthT);
dq_ref(1) = qf - qi;


