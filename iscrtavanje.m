% crtanje unutrasnjih koordinata
figure
subplot(3,2,1)
plot(Ps.t,Ps.q_ref(:,1),Ps.t,Ps.q(:,1),'r','LineWidth',2)
grid;
title('Unutrasnja koordinata q1-pozicija prvog zgloba', 'FontSize', 20);
xlabel('vreme [s]', 'FontSize', 20);
ylabel('stepeni [dg]', 'FontSize', 20);
legend('referentana vrednost','ostvarena vrednost');

subplot(3,2,2)
plot(Ps.t,Ps.q_ref(:,2),Ps.t,Ps.q(:,2),'r','LineWidth',2)
grid;
title('Unutrasnja koordinata q2-pozicija drugog zgloba', 'FontSize', 20);
xlabel('vreme [s]', 'FontSize', 20);
ylabel('stepeni [dg]', 'FontSize', 20);
legend('referentana vrednost','ostvarena vrednost');

subplot(3,2,3)
plot(Ps.t,Ps.q_ref(:,3),Ps.t,Ps.q(:,3),'r','LineWidth',2)
grid;
title('Unutrasnja koordinata q3-pozicija prvog zgloba', 'FontSize', 20);
xlabel('vreme [s]', 'FontSize', 20);
ylabel('stepeni [dg]', 'FontSize', 20);
legend('referentana vrednost','ostvarena vrednost');

subplot(3,2,4)
plot(Ps.t,Ps.q_ref(:,4),Ps.t,Ps.q(:,4),'r','LineWidth',2)
grid;
title('Unutrasnja koordinata q4-pozicija drugog zgloba','FontSize', 20);
xlabel('vreme [s]', 'FontSize', 20);
ylabel('stepeni [dg]', 'FontSize', 20);
legend('referentana vrednost','ostvarena vrednost');

subplot(3,2,5)
plot(Ps.t,Ps.q_ref(:,5),Ps.t,Ps.q(:,5),'r','LineWidth',2)
grid;
title('Unutrasnja koordinata q5-pozicija prvog zgloba', 'FontSize', 20);
xlabel('vreme [s]', 'FontSize', 20);
ylabel('stepeni [dg]', 'FontSize', 20);
legend('referentana vrednost','ostvarena vrednost');

subplot(3,2,6)
plot(Ps.t,Ps.q_ref(:,6),Ps.t,Ps.q(:,6),'r','LineWidth',2)
grid;
title('Unutrasnja koordinata q6-pozicija drugog zgloba', 'FontSize', 20);
xlabel('vreme [s]', 'FontSize', 20);
ylabel('stepeni [dg]', 'FontSize', 20);
legend('referentana vrednost','ostvarena vrednost');


% crtanje brzina unutrasnjih koordinata
figure
subplot(3,2,1)
plot(Ps.t,Ps.dq_ref(:,1),Ps.t,Ps.dq(:,1),'r','LineWidth',2)
grid;
title('Unutrasnja koordinata dq1-brzina prvog zgloba');
xlabel('vreme [s]');
ylabel('stepeni/sekund [dg/s]');
legend('referentana vrednost','ostvarena vrednost');

subplot(3,2,2)
plot(Ps.t,Ps.dq_ref(:,2),Ps.t,Ps.dq(:,2),'r','LineWidth',2)
grid;
title('Unutrasnja koordinata dq2-brzina drugog zgloba');
xlabel('vreme [s]');
ylabel('stepeni/sekund [dg/s]');
legend('referentana vrednost','ostvarena vrednost');

subplot(3,2,3)
plot(Ps.t,Ps.dq_ref(:,3),Ps.t,Ps.dq(:,3),'r','LineWidth',2)
grid;
title('Unutrasnja koordinata dq3-brzina prvog zgloba');
xlabel('vreme [s]');
ylabel('stepeni/sekund [dg/s]');
legend('referentana vrednost','ostvarena vrednost');

subplot(3,2,4)
plot(Ps.t,Ps.dq_ref(:,4),Ps.t,Ps.dq(:,4),'r','LineWidth',2)
grid;
title('Unutrasnja koordinata dq4-brzina drugog zgloba');
xlabel('vreme [s]');
ylabel('stepeni/sekund [dg/s]');
legend('referentana vrednost','ostvarena vrednost');

subplot(3,2,5)
plot(Ps.t,Ps.dq_ref(:,5),Ps.t,Ps.dq(:,5),'r','LineWidth',2)
grid;
title('Unutrasnja koordinata dq5-brzina prvog zgloba');
xlabel('vreme [s]');
ylabel('stepeni/sekund [dg/s]');
legend('referentana vrednost','ostvarena vrednost');

subplot(3,2,6)
plot(Ps.t,Ps.dq_ref(:,6),Ps.t,Ps.dq(:,6),'r','LineWidth',2)
grid;
title('Unutrasnja koordinata dq6-brzina drugog zgloba');
xlabel('vreme [s]');
ylabel('stepeni/sekund [dg/s]');
legend('referentana vrednost','ostvarena vrednost');


% crtanje spoljasnjih koordinata
figure
subplot(1,3,1)
plot(Ps.t,Ps.X_ref(:,1),Ps.t,Ps.X(:,1),'r','LineWidth',2)
grid;
title('Spoljasnja koordinata x-pozicija na x osi');
xlabel('vreme [s]');
ylabel('pozicija [m]');
legend('referentana vrednost','ostvarena vrednost');

subplot(1,3,2)
plot(Ps.t,Ps.X_ref(:,2),Ps.t,Ps.X(:,2),'r','LineWidth',2)
grid;
title('Spoljasnja koordinata y-pozicija na y osi');
xlabel('vreme [s]');
ylabel('pozicija [m]');
legend('referentana vrednost','ostvarena vrednost');

subplot(1,3,3)
plot(Ps.t,Ps.X_ref(:,3),Ps.t,Ps.X(:,3),'r','LineWidth',2)
grid;
title('Spoljasnja koordinata z-pozicija na z osi');
xlabel('vreme [s]');
ylabel('pozicija [m]');
legend('referentana vrednost','ostvarena vrednost');


% crtanje brzina spoljasnjih koordinata
figure
subplot(1,3,1)
plot(Ps.t,Ps.dX_ref(:,1),Ps.t,Ps.dX(:,1),'r','LineWidth',2)
grid;
title('Spoljasnja kordinata dx-x komponenta brzine');
xlabel('vreme [s]');
ylabel('brzina [m/s]');
legend('referentana vrednost','ostvarena vrednost');

subplot(1,3,2)
plot(Ps.t,Ps.dX_ref(:,2),Ps.t,Ps.dX(:,2),'r','LineWidth',2)
grid;
title('Spoljasnja kordinata dy-y komponenta brzine');
xlabel('vreme [s]');
ylabel('brzina [m/s]');
legend('referentana vrednost','ostvarena vrednost');

subplot(1,3,3)
plot(Ps.t,Ps.dX_ref(:,3),Ps.t,Ps.dX(:,3),'r','LineWidth',2)
grid;
title('Spoljasnja kordinata dz-z komponenta brzine');
xlabel('vreme [s]');
ylabel('brzina [m/s]');
legend('referentana vrednost','ostvarena vrednost');


% crtanje pogonskih momenata
figure
subplot(3,2,1)
plot(Ps.t,Ps.Tau(:,1),'LineWidth',2)
grid;
title('Ostvareni pogonski moment prvog zgloba');
xlabel('vreme [s]');
ylabel('moment [Nm]');

subplot(3,2,2)
plot(Ps.t,Ps.Tau(:,2),'LineWidth',2)
grid;
title('Ostvareni pogonski moment drugog zgloba');
xlabel('vreme [s]');
ylabel('moment [Nm]');

subplot(3,2,3)
plot(Ps.t,Ps.Tau(:,3),'LineWidth',2)
grid;
title('Ostvareni pogonski moment treceg zgloba');
xlabel('vreme [s]');
ylabel('moment [Nm]');

subplot(3,2,4)
plot(Ps.t,Ps.Tau(:,4),'LineWidth',2)
grid;
title('Ostvareni pogonski moment cetvrtog zgloba');
xlabel('vreme [s]');
ylabel('moment [Nm]');

subplot(3,2,5)
plot(Ps.t,Ps.Tau(:,5),'LineWidth',2)
grid;
title('Ostvareni pogonski moment petog zgloba');
xlabel('vreme [s]');
ylabel('moment [Nm]');

subplot(3,2,6)
plot(Ps.t,Ps.Tau(:,6),'LineWidth',2)
grid;
title('Ostvareni pogonski moment sestog zgloba');
xlabel('vreme [s]');
ylabel('moment [Nm]');


% crtanje pogonskih momenata feedforward
figure
subplot(3,2,1)
plot(Ps.t,Ps.Tau_FF(:,1),'LineWidth',2)
grid;
title('feedforward vrednost upravljanja za prvi zglob');
xlabel('vreme [s]');
ylabel('Momenat');

subplot(3,2,2)
plot(Ps.t,Ps.Tau_FF(:,2),'LineWidth',2)
grid;
title('feedforward vrednost upravljanja za drugi zglob');
xlabel('vreme [s]');
ylabel('Momenat');

subplot(3,2,3)
plot(Ps.t,Ps.Tau_FF(:,3),'LineWidth',2)
grid;
title('feedforward vrednost upravljanja za treci zglob');
xlabel('vreme [s]');
ylabel('Momenat');

subplot(3,2,4)
plot(Ps.t,Ps.Tau_FF(:,4),'LineWidth',2)
grid;
title('feedforward vrednost upravljanja za cetvrti zglob');
xlabel('vreme [s]');
ylabel('Momenat');

subplot(3,2,5)
plot(Ps.t,Ps.Tau_FF(:,5),'LineWidth',2)
grid;
title('feedforward vrednost upravljanja za peti zglob');
xlabel('vreme [s]');
ylabel('Momenat');

subplot(3,2,6)
plot(Ps.t,Ps.Tau_FF(:,6),'LineWidth',2)
grid;
title('feedforward vrednost upravljanja za sesti zglob');
xlabel('vreme [s]');
ylabel('Momenat');


% crtanje pogonskih momenata feedback
figure
subplot(3,2,1)
plot(Ps.t,Ps.Tau_FB(:,1),'LineWidth',2)
grid;
title('feedback vrednost upravljanja za prvi zglob');
xlabel('vreme [s]');
ylabel('Momenat');

subplot(3,2,2)
plot(Ps.t,Ps.Tau_FB(:,2),'LineWidth',2)
grid;
title('feedback vrednost upravljanja za drugi zglob');
xlabel('vreme [s]');
ylabel('Momenat');

subplot(3,2,3)
plot(Ps.t,Ps.Tau_FB(:,3),'LineWidth',2)
grid;
title('feedback vrednost upravljanja za treci zglob');
xlabel('vreme [s]');
ylabel('Momenat');

subplot(3,2,4)
plot(Ps.t,Ps.Tau_FB(:,4),'LineWidth',2)
grid;
title('feedback vrednost upravljanja za cetvrti zglob');
xlabel('vreme [s]');
ylabel('Momenat');

subplot(3,2,5)
plot(Ps.t,Ps.Tau_FB(:,5),'LineWidth',2)
grid;
title('feedback vrednost upravljanja za peti zglob');
xlabel('vreme [s]');
ylabel('Momenat');

subplot(3,2,6)
plot(Ps.t,Ps.Tau_FB(:,6),'LineWidth',2)
grid;
title('feedback vrednost upravljanja za sesti zglob');
xlabel('vreme [s]');
ylabel('Momenat');
