Ps.t(i) = t_tmp;                        % vreme [s]    
Ps.q(i,:) = q.*(180/pi);                % pozicje zglobova [dg]
Ps.dq(i,:) = dq.*(180/pi);              % brzine zglobova [dg/s]
Ps.q_ref(i,:) = q_ref(i,:).*(180/pi);   % referentna pozicija zglobova [dg]
Ps.dq_ref(i,:) = dq_ref(i,:).*(180/pi); % referentna brzina zglobova [dg/s]
Ps.X(i,:) = X_value;                    % Dekartove kordinate [m]
Ps.X_ref(i,:) = X_ref_value;            % referenntne Dekartove koedinate [m]
Ps.dX(i,:) = dX_value;                  % brzine u dekartovim kordinatama X
Ps.dX_ref(i,:) = dX_ref_value;          % brzine u dekartovim kordinatama X_ref

Ps.Tau(i,:) = Tau';                     %  ostvareni moment koji reduktor predaje sistemu [Nm]
Ps.Tau_FF(i,:) = Tau_FF';               %  upravljacki signal-struja [A] FF komponenta
Ps.Tau_FB(i,:) = Tau_FB';               % upravljacki signal-struja [A] FB komponenta